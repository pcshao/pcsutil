package cn.pcshao.util.pcsutil.compress;

import java.util.List;

/**
 * @author pcshao.cn
 * @date 2022-09-12
 */
public class CompressUtils {

    public static void main(String[] args) {
        System.out.println(compress("ABCABDEBC", 5, 2));
    }

    public static String compress(String inputStr, int sizeLeft, int sizeRight) {
        StringBuilder result = new StringBuilder();
        int len = inputStr.length();
        for (int i = 0; i < len; i++) {
            // 首位处理
            if (i == 0) {
                String strRight = inputStr.substring(i, sizeRight);
                result.append("00").append(strRight.charAt(0)).append(",");
                continue;
            }
            String strRight = inputStr.substring(i, min(i + sizeRight, len));
            String strNext = inputStr.substring(min(i + sizeRight, len), min(i + sizeRight + 1, len));
            char charNext = strNext.length() > 0 ? strNext.charAt(0) : strRight.charAt(strRight.length() - 1);
            String strLeft = inputStr.substring(max(i - sizeLeft, 0), i);

            int index = strLeft.indexOf(strRight);
            if (index > -1) {
                int i1 = i - index;
                int i2 = strRight.length();
                i += i2;
                result.append(String.format("%d%d%c", i1, i2, charNext));
            } else {
                result.append("00").append(strRight.charAt(0));
            }
            result.append(",");
        }
        result.delete(result.length() - 1, result.length());
        return result.toString();
    }

    private static int min(int i, int j) {
        return i < j ? i : j;
    }

    private static int max(int i, int j) {
        return i > j ? i : j;
    }

    public static String deCompress(String compressStr) {

        return null;
    }

}
