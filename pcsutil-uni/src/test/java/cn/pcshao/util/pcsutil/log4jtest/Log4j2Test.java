package cn.pcshao.util.pcsutil.log4jtest;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.async.AsyncLoggerContextSelector;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.task.TaskExecutor;

import javax.annotation.Resource;

/**
 * @author pcshao.cn
 * @date 2022/4/18
 **/
@Slf4j
@SpringBootTest
public class Log4j2Test {

    @Resource
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    class MyRunnable implements Runnable {
        private int count;
        public MyRunnable(int count) {
            this.count = count;
        }

        @Override
        public void run() {
            System.out.println(String.format("start %d %s", count, Thread.currentThread().getName()));
            log.info("这是info count:{} 是否异步:{} ", count, AsyncLoggerContextSelector.isSelected());
            log.error("这是error count:{} 是否异步:{} ", count, AsyncLoggerContextSelector.isSelected());
            log.debug("这是debug count:{} 是否异步:{} ", count, AsyncLoggerContextSelector.isSelected());
            System.out.println(String.format("end %d %s", count, Thread.currentThread().getName()));
        }
    }

    @Test
    public void test() {
        // test
        for (int i = 0; i < 5; i++) {
            taskExecutor.execute(new MyRunnable(i));
        }
    }
}
