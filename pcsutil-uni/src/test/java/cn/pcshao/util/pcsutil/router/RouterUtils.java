package cn.pcshao.util.pcsutil.router;

/**
 * @author pcshao.cn
 * @date 2022-09-20
 */
public class RouterUtils {

    public static void main(String[] args) {
        System.out.println(routerMatch("300203", "300???"));
        System.out.println(routerMatch("300203", "300*"));
        System.out.println(routerMatch("300203", "3001??"));
        System.out.println(routerMatch("300203", "300*3"));
        System.out.println(routerMatch("300203", "300*03"));
    }

    public static boolean routerMatch(String functionStr, String pattern) {
        boolean lenMatch = functionStr.length() == pattern.length();
        boolean lastStar = false;
        int lastPos = 0;
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            if (c == '*') {
                // 首位*通配所有，末位*通配所有
                if (i == 0 || i + 1 == pattern.length()) {
                    return true;
                }
                //
                if (!lastStar) {
                    lastStar = true;
                    continue;
                } else {
                    // 连续两个*
                    continue;
                }
            }
            if (c == '?') {
                // 如果规则串末位为?且也是功能串末位，则返回true
                if (i + 1 == pattern.length() && lenMatch) {
                    return true;
                } else {
                    continue;
                }
            }
            // 规则串已走完整个功能串
            if (i + 1 > functionStr.length()) {
                // 如果当前位为*
                if (c == '*') {
                    return true;
                } else {
                    return false;
                }
            }
            // 以上都走完，则进行强匹配
            if (lastStar) {
                // 上一个字符是*，找规则串后面字符是否在功能串中存在，是则记录，否则返回false
                boolean find = false;
                if (lastPos > 0) {
                    // 如果之前找过一轮已找到，则这一轮只需要找一个字符
                    if (++lastPos < functionStr.length()) {
                        if (functionStr.charAt(lastPos) == c) {
                            // 如果到最后一位，直接返回true
                            if (i + 1 == pattern.length()) {
                                return true;
                            }
                            continue;
                        } else {
                            return false;
                        }
                    }
                } else {
                    for (int j = i - 1; j < functionStr.length(); j++) {
                        if (functionStr.charAt(j) == c) {
                            find = true;
                            lastPos = j;
                            break;
                        }
                    }
                    if (find) {
                        // 如果找到了，且是规则串最后一位，返回true，否则继续找
                        if (i + 1 == pattern.length()) {
                            return true;
                        }
                        continue;
                    } else {
                        return false;
                    }
                }
            } else if (functionStr.charAt(i) == c) {
                continue;
            } else {
                return false;
            }
        }
        return false;
    }

    private static int max(int a, int b) {
        return a > b ? a : b;
    }
}
