package cn.pcshao.util.pcsutil.creshelper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.task.TaskExecutor;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;

@SpringBootTest
public class CresHelperTest {

    @Resource
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    @Autowired
    CresHelper cresHelper;

    @Test
    public void fileMonitor() throws IOException, InterruptedException {
        String basePath = "C:\\generate";
        //文件监听服务
        WatchService watchService = null;
        watchService = FileSystems.getDefault().newWatchService();
        //给文件路径注册监听服务
        Paths.get(basePath)
                .register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
        //变动的目录
        List<String> modifyPathList = new LinkedList<>();
        while (true){
            WatchKey watchKey = watchService.take();
            List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
            for (WatchEvent event : watchEvents){
                if (StandardWatchEventKinds.ENTRY_MODIFY == event.kind() || StandardWatchEventKinds.ENTRY_CREATE == event.kind()){
                    String temp = event.context().toString();
                    if (temp.split(".").length == 0)
                        modifyPathList.add(basePath+ "/"+ temp+ "/");
                }
            }
            watchKey.reset();
            //稍微休眠
            Thread.sleep(500);
        }
    }
    @Test
    void contextLoads() {
        taskExecutor.execute(()->{
            CrTask crTask = new CrTask("\"C:\\\\generate", "编译完成SO文件");
            cresHelper.execute(crTask);
            Thread.interrupted();
        });
        taskExecutor.execute(()->{
            CrTask crTask = new CrTask("\"C:\\\\generate", "编译完成SO文件");
            cresHelper.execute(crTask);
            Thread.interrupted();
        });
        try {
            Thread.sleep(1000*60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testTaskExecutor(){
        taskExecutor.execute(() -> {
            System.out.println("Real thread begin to execute!");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("Real thread was interrupted!");
                return;
            }
            System.out.println("Real thread has been executed!");
        });
    }
    @Test
    void cresHelperTest(){
        cresHelper.execute();
    }
}
