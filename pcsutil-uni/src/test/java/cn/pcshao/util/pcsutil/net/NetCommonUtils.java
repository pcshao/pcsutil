package cn.pcshao.util.pcsutil.net;

import com.sun.org.apache.regexp.internal.RE;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author pcshao.cn
 * @date 2022-09-11
 */
public class NetCommonUtils {

    public static void main(String[] args) {
        int iloop = 100;

        long start = System.currentTimeMillis();
        for (int i = 0; i < iloop; i++) {
            judgeIpvTypeByNetUtil("192.168.123.1");
        }
        System.out.println(System.currentTimeMillis() - start);

        long start1 = System.currentTimeMillis();
        for (int i = 0; i < iloop; i++) {
            judgeIpvType("192.168.123.1");
        }
        System.out.println(System.currentTimeMillis() - start1);

        long start2 = System.currentTimeMillis();
        for (int i = 0; i < iloop; i++) {
            judgeIpvType("0A1:2E:6B:77:80d:12e:1cc:6aa1");
        }
        System.out.println(System.currentTimeMillis() - start2);

        long start4 = System.currentTimeMillis();
        for (int i = 0; i < iloop; i++) {
            judgeIpvTypeByNetUtil("0A1:2E:6B:77:80d:12e:1cc:6aa1");
        }
        System.out.println(System.currentTimeMillis() - start);

    }

    private static String judgeIpvTypeByNetUtil(String ip) {
        try {
            InetAddress byName = InetAddress.getByName(ip);
            if (byName instanceof Inet6Address) {
                return "IPV6";
            } else if (byName instanceof Inet4Address) {
                return "IPV4";
            } else {
                return "ERROR";
            }
        } catch (UnknownHostException e) {
            return "ERROR";
        }
    }

    public static String judgeIpvType(String ip) {
        String ERROR = "ERROR";
        String IPV4 = "IPV4";
        String IPV6 = "IPV6";
        if (ip.indexOf(".") > -1) {
            String[] ipv4 = ip.split("\\.");
            if (ipv4.length == 4) {
                // IPV4
                for (String item : ipv4) {
                    try {
                        int number = Integer.parseInt(item);
                        if (!(0 <= number && number <= 255)) {
                            return ERROR;
                        }
                    } catch (NumberFormatException e) {
                        return ERROR;
                    }
                }
                return IPV4;
            }
        } else if (ip.indexOf(":") > -1){
            String[] ipv6 = ip.split(":");
            if (ipv6.length == 8) {
                // IPV6
                for (String item : ipv6) {
                    if (!(1 <= item.length() && item.length() <= 4)) {
                        return ERROR;
                    }
                    try {
                        Integer.parseInt(item, 16);
                    } catch (NumberFormatException e) {
                        return ERROR;
                    }
                }
                return IPV6;
            }
        }
        return ERROR;
    }

}
