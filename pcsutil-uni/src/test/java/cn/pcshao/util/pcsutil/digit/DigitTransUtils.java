package cn.pcshao.util.pcsutil.digit;

import java.util.Scanner;

/**
 * @author pcshao.cn
 * @date 2022-09-11
 */
public class DigitTransUtils {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println(BaseNtoBase10("a", 16));
            // Integer.parseInt("-1A1", 16);
            System.out.println(Base10toBaseN(10, 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long BaseNtoBase10(String digitStr, int digit) throws Exception {
        // return Integer.parseInt(digitStr, digit);
        // 入参基础校验
        if (null == digitStr) {
            throw new Exception("入参进制串不能为null");
        }
        if (digit < 2 || digit > 36) {
            throw new Exception("进制超限");
        }
        // 进制串首位校验
        boolean isNegative = false;
        // int limit = -Integer.MAX_VALUE;
        int inputLen = digitStr.length();
        int i = 0;
        int result = 0;
        if (inputLen > 0) {
            char firstChar = digitStr.charAt(0);
            if (firstChar < '0') {
                if (firstChar == '-') {
                    isNegative = true;
                    // limit = Integer.MIN_VALUE;
                } else if (firstChar != '+') {
                    throw new Exception("入参进制串首位不合法");
                }
                if (inputLen == 1) {
                    throw new Exception("入参进制串不合法");
                }
                i++;
            }
            while (i < inputLen) {
                // TODO 其他进制转
                // int charNumber = Character.digit(digitStr.charAt(i++), digit);
                int charNumber = calcDigitNumber(digitStr.charAt(i++), digit);
                if (charNumber < 0) {
                    throw new Exception("不合法");
                }
                result *= digit;
                // if (result - charNumber < limit) {
                // if (result < limit + charNumber) {
                //     throw new Exception("超限");
                // }
                result -= charNumber;
            }
        }
        return isNegative ? result : -result;
    }

    public int Nto10(String inputStr, int digit) throws Exception {
        boolean isNegative = false;
        if (inputStr.charAt(0) == '-') {
            isNegative = true;
        } else if (inputStr.charAt(0) == '+') {
            inputStr = inputStr.substring(1);
        }
        int result = 0;
        for (int i = 0; i < inputStr.length(); i++) {
            int number = calc(inputStr.charAt(0), digit);
            result *= digit;
            result -= number;
        }
        return isNegative ? result : -result;
    }

    private int calc(char charAt, int digit) throws Exception {
        if ('0' <= charAt && charAt <= '9') {
            return (int) Math.pow(charAt - '0', digit);
        } else if ('A' <= charAt && charAt <= 'Z') {
            return (int) Math.pow(charAt - 'A', digit);
        } else if ('a' <= charAt && charAt <= 'z') {
            return (int) Math.pow(charAt - 'a', digit);
        } else {
            throw new Exception("输入异常");
        }
    }

    public String base10ToN (int number, int digit) {
        boolean isNegative = number < 0;
        if (!isNegative) {
            number = -number;
        }
        int pos = 32;
        char[] buff = new char[33];
        for (; pos >= -number; pos--) {
            // 取模进制放入buff
            buff[pos--] = digits[-(number % digit)];
            // 原数除以进制
            number /= digit;
        }
        // 首位原数转进制数
        buff[--pos] = digits[-number];
        return new String(buff, pos, 33 - pos);
    }

    private static int calcDigitNumber(char charAt, int digit) throws Exception {
        if ('0' <= charAt && charAt <= '9') {
            return (int) Math.pow(((int)charAt - '0'), digit);
        } else if ('A' <= charAt && charAt <= 'Z') {
            return 10 + (int) Math.pow(((int)charAt - 'A'), digit);
        } else if ('a' <= charAt && charAt <= 'z') {
            return 26 + (int) Math.pow(((int)charAt - 'a'), digit);
        } else {
            throw new Exception("进制串非法");
        }
    }

    final static char[] digits = {
            '0' , '1' , '2' , '3' , '4' , '5' ,
            '6' , '7' , '8' , '9' , 'a' , 'b' ,
            'c' , 'd' , 'e' , 'f' , 'g' , 'h' ,
            'i' , 'j' , 'k' , 'l' , 'm' , 'n' ,
            'o' , 'p' , 'q' , 'r' , 's' , 't' ,
            'u' , 'v' , 'w' , 'x' , 'y' , 'z'
    };

    public static String Base10toBaseN(int originNumber, int digit) throws Exception {
        // return Integer.toString(originNumber, digit);
        if (digit < 2 || digit > 36) {
            throw new Exception("进制超限");
        }
        if (digit == 10) {
            return String.valueOf(originNumber);
        }

        boolean negative = (originNumber < 0);
        if (!negative) {
            originNumber = -originNumber;
        }

        int bufSize = 33;    // 给出符号位
        char buf[] = new char[bufSize];
        int charPos = 32;

        while (originNumber <= -digit) {
            buf[charPos--] = digits[-(originNumber % digit)];
            originNumber /= digit;
        }
        buf[charPos] = digits[-originNumber];

        if (negative) {
            buf[--charPos] = '-';
        }

        return new String(buf, charPos, (bufSize - charPos));
    }
}
