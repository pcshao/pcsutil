package cn.pcshao.util.pcsutil.exam;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author pcshao.cn
 * @date 2022-09-20
 */
public class StockMarketTest {

    public static void marketTest() throws Exception {
        Scanner scanner = new Scanner(System.in);
        int beginBalance = scanner.nextInt();
        int days = scanner.nextInt();
        String priceInput = scanner.next();
        String[] split = priceInput.split(",");
        if (split.length != days) {
            throw new Exception("入参价格天数与价格参数不符！" + split);
        }
        ArrayList priceList = new ArrayList();
        for (String item : split) {
            try {
                priceList.add(Integer.parseInt(item));
            } catch (Exception e) {
                throw new Exception("价格参数不符！" + item);
            }
        }
        System.out.println("最大获利 = " + maxProfit(priceList, beginBalance));
    }

    public static void main(String[] args) {
        int[] prices = new int[]{2,1,7,3,4}; // 场景1.有利润场景
        // prices = new int[]{7,4,3,2,1}; // 场景2.无利润场景
        double balance = 10000D;
        try {
            marketTest();
        } catch (Exception e) {

        }
    }

    public static int maxProfit(List<Integer> prices, double balance) {
        int minprice = Integer.MAX_VALUE;
        // 单只股票最大收益
        int maxprofit = 0;
        // 最大买入数量
        int maxamount = 0;
        for (int i = 0; i < prices.size(); i++) {
            // 当前价位小于最小价，最小价得出
            if (prices.get(i) < minprice) {
                minprice = prices.get(i);
            } else if (prices.get(i) - minprice > maxprofit) {
                maxprofit = prices.get(i) - minprice;
                maxamount = (int) (balance / minprice);
            }
        }
        // 最大利润 = 单只最大收益 * 最大数量
        return maxprofit * maxamount;
    }
}
