package cn.pcshao.util.pcsutil.spider;

import cn.pcshao.oa.common.util.StringUtils;
import cn.pcshao.util.pcsutil.MenuExecuteInterface;
import cn.pcshao.util.pcsutil.utils.DingTalkUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pcshao.cn
 * @date 2021-03-21
 */
@Service
public class HsHomeSpider implements MenuExecuteInterface {

    private static Logger logger = LoggerFactory.getLogger(HsHomeSpider.class);

    private String username;
    private String password;
    private String token = "ASP.NET_SessionId=33kga2f2kyan54cn4zkakxei";
    private String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36";
    private int time = 10*60*1000;
    private int pageNum;
    String casDomain = "https://hs-cas.hundsun.com";
    String homeDomain = "https://home.hundsun.com/";
    private String rootUrl = "/bar/s-69.aspx";
    private int mincost, maxcost;
    private String keyword;

    private Jedis jedis;
    public boolean redisSwitch = false;
    private String redisUrl;
    private int redisPort;
    private int redisDb;
    private String redisPassword;
    private long expireSecond;

    @Autowired
    private DingTalkUtil dingTalkUtil;
    private Set messageYetSet;

    public HsHomeSpider(@Value("${hsspider.username}") String username,
                        @Value("${hsspider.password}") String password,
                        @Value("${hsspider.token}") String token,
                        @Value("${hsspider.time}") String time,
                        @Value("${hsspider.casDomain}") String casDomain,
                        @Value("${hsspider.homeDomain}") String homeDomain,
                        @Value("${hsspider.rootUrl}") String rootUrl,
                        @Value("${hsspider.mincost}") String mincost,
                        @Value("${hsspider.maxcost}") String maxcost,
                        @Value("${hsspider.keyword}") String keyword,
                        @Value("${hsspider.pageNum}") String pageNum,
                        @Value("${hsspider.userAgent}") String userAgent,
                        @Value("${hsspider.redis.switch}") boolean redisSwitch,
                        @Value("${hsspider.redis.url}") String redisUrl,
                        @Value("${hsspider.redis.port}") int redisPort,
                        @Value("${hsspider.redis.db}") int redisDb,
                        @Value("${hsspider.redis.password}") String redisPassword,
                        @Value("${hsspider.redis.expire-second}") long expireSecond) {
        this.username = username;
        this.password = password;
        this.token = token;
        this.time = Integer.parseInt(time);
        this.pageNum = Integer.parseInt(pageNum);
        this.casDomain = casDomain;
        this.homeDomain = homeDomain;
        this.rootUrl = rootUrl;
        this.mincost = Integer.parseInt(mincost);
        this.maxcost = Integer.parseInt(maxcost);
        this.keyword = keyword;
        this.userAgent = userAgent;
        this.redisSwitch = redisSwitch;
        this.redisUrl = redisUrl;
        this.redisPort = redisPort;
        this.redisDb = redisDb;
        this.redisPassword = redisPassword;
        this.expireSecond = expireSecond;
    }

    public void execute() {
        List<String> cookies = new ArrayList<>();
        // 登录
        try {
            logger.info("开始登录...");
            login(casDomain, homeDomain, this.token, this.userAgent, cookies);
        } catch (Exception e) {
            logger.error("登录失败！", e);
            return;
        }
        // cookies
        String token = cookies.stream().collect(Collectors.joining(";"));
        logger.info("登录成功！用户:{}，cookie:{}", username, token);
        Dao dao = new Dao();
        while (true) {
            logger.info("给爷爬！");
            spide(dao, homeDomain, token.toString(), this.userAgent);
            // 推送目标数据
            pushMessage(dao, mincost, maxcost, keyword);
            // sleep
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
            }
        }
    }

    private void spide(Dao dao, String homeDomain, String token, String userAgent) {
        for (int i = 1; i <= pageNum; i++) {
            int curr = i;
            try {
                doSpide(dao, homeDomain, token, curr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void doSpide(Dao dao, String homeDomain, String token, int page) throws IOException {
        Document doc = Jsoup.connect(homeDomain + rootUrl)
                .userAgent(userAgent)
                .header("Cookie", token.toString())
                .data("pageIndex", String.valueOf(page))
                .get();
        // load
        if (doc != null) {
            Element barThreadsContainer = doc.getElementById("barThreadsContainer");
            if (barThreadsContainer != null) {
                Elements items = barThreadsContainer.getElementsByAttributeValue("class", "tn-list-item-area-main");
                for (Element item : items) {
                    Goods goods = new Goods();
                    Element h5 = item.getElementsByTag("h5").first();
                    Elements hrefs = h5.getElementsByAttribute("href");
                    for (int m = 0; m < hrefs.size(); m++) {
                        if (m == 0) {
                            goods.setType(hrefs.get(m).text());
                        } else if (m == 1) {
                            goods.setTitle(hrefs.get(m).text());
                            goods.setHref(homeDomain + hrefs.get(m).attr("href"));
                        }
                    }
                    dao.put(goods.getHref(), goods);
                }
            }
        }
    }

    /**
     * 钉钉消息推送
     * @param message
     * @param pour
     */
    private void sendDingDingMessage(String message, boolean pour) {
        // 开启redis时不使用本地已推送消息集合
        if (this.redisSwitch) {
            if (!messageYetRedis(message, this.redisDb) || pour) {
                dingTalkUtil.sendMessageQueue(new DingTalkUtil.Message().setMessage(message), pour);
            }
        } else {
            // 推送消息时支持去重
            if (Objects.isNull(messageYetSet))
                messageYetSet = new HashSet();
            if (!messageYetSet.contains(message) || pour) {
                messageYetSet.add(message);
                dingTalkUtil.sendMessageQueue(new DingTalkUtil.Message().setMessage(message), pour);
            }
        }
    }

    private boolean messageYetRedis(String message, int db) {
        if (Objects.isNull(jedis)) {
            jedis = new Jedis(this.redisUrl, this.redisPort);
            // 20231014 pcshao add 增加密码访问
            if (StringUtils.isNotBlank(this.redisPassword))
                jedis.auth(this.redisPassword);
            jedis.select(db);
            logger.info(jedis.info());
        }
        String origMessage = jedis.get(message);
        // 不存在则存入redis并返回false不存在
        if (Objects.isNull(origMessage)) {
            // jedis.set(message, "1");
            jedis.set(message, "1", new SetParams().ex(expireSecond));
            return false;
        }
        return true;
    }

    /**
     * 推送信息
     *  1.日志消息
     *  2.钉钉消息
     * @param dao
     * @param MIN_COST
     * @param MAX_COST
     * @param keyword
     */
    private void pushMessage(Dao dao, int MIN_COST, int MAX_COST, String keyword) {
        sendDingDingMessage("--推送开始--", false);
        // 匹配价格
        for (Map.Entry<String, Goods> entry : dao.datas.entrySet()) {
            Goods goods = entry.getValue();
            boolean underCost = false;
            if (goods.getTitle() != null) {
                // 过滤 已收 已出
                final int pos = goods.getTitle().indexOf("已");
                if (pos > -1 && goods.getTitle().length() > (pos + 1)) {
                    char next = goods.getTitle().charAt(pos + 1);
                    if (('出' == next) || ('收' == next) || ('转' == next))
                        continue;
                }
                // 关键字
                if (!keyword.trim().equals("") && goods.getTitle().contains(keyword)) {
                    logger.info("【关键字{}命中】" + goods.getTitle() + "-->" + goods.getHref(), keyword);
                    String dMessage = goods.getTitle()+"\n "+goods.getHref()+"\n";;
                    sendDingDingMessage(dMessage, false);
                    continue;
                }
                // 数字
                char[] chars = new String(goods.getTitle() + "e").toCharArray();    //最后一位补非数字
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < chars.length; i++) {
                    if (48 <= chars[i] && chars[i] <= 57) {
                        sb.append(chars[i]);
                        underCost = true;
                    } else if (underCost) {
                        try{
                            double cost = Double.parseDouble(sb.toString());
                            if (MIN_COST <= cost && cost <= MAX_COST) {
                                break;
                            } else {
                                sb.delete(0, sb.length());
                                underCost = false;
                            }
                        } catch (Exception e) {
                            sb.delete(0, sb.length());
                            underCost = false;
                        }
                    }
                }
                if (underCost) {
                    logger.info("【包含数字区间[{},{}]】" + goods.getTitle() + "-->" + goods.getHref(), MIN_COST, MAX_COST);
                    String dMessage = goods.getTitle()+"\n "+goods.getHref()+"\n";
                    sendDingDingMessage(dMessage, false);
                }
            }
        }
        sendDingDingMessage("--单次推送结束，下一次推送在"+(time/1000)+"s后--", true);
    }

    private void login(String casDomain, String homeDomain, String token, String userAgent, List<String> cookies) throws Exception {
        String sessionName = "JSESSIONID";
        String session2Name = "sto-id-47873";
        String ASPXAUTH = ".ASPXAUTH";

        // String token = "ASP.NET_SessionId=33kga2f2kyan54cn4zkakxei";
        Connection.Response response = Jsoup.connect("https://home.hundsun.com/u/26912.aspx")
                .userAgent(userAgent)
                .header("Cookie", token)
                .execute();
        Document docLogin = response.parse();
        String sessionId = response.cookie(sessionName);
        String session2Id = response.cookie(session2Name);
        String action = docLogin.getElementById("fm1").attr("action");
        String lt = docLogin.getElementById("bottom").getElementsByAttributeValue("name", "lt").first().attr("value");
        String ex = docLogin.getElementById("bottom").getElementsByAttributeValue("name", "execution").first().attr("value");

        Connection.Response loginResp = Jsoup.connect(casDomain + action)
                .userAgent(userAgent)
                .header("Cookie", sessionName + "=" + sessionId + ";" + session2Name + "=" + session2Id)
                .data("username", username, "password", password, "lt", lt, "execution", ex, "_eventId", "submit")
                .method(Connection.Method.POST)
                .followRedirects(false)
                .execute();
        String reditUrl = loginResp.header("Location");

        if (reditUrl == null)
            throw new Exception("重定向页面为空！");
        Connection.Response login1Resp = Jsoup.connect(reditUrl)
                .userAgent(userAgent)
                .method(Connection.Method.POST)
                .header("Cookie", token)
                .followRedirects(false)
                .execute();
        String ASPXAUTH_VALUE = login1Resp.cookie(ASPXAUTH);
        cookies.add(ASPXAUTH + "=" + ASPXAUTH_VALUE);
        cookies.add(token);
    }
}
class Dao{
    Map<String, Goods> datas = new HashMap<>();

    public void put(String key, Goods goods) {
        datas.put(key, goods);
    }
}
class Goods{
    private String type;
    private String title;
    private String href;
    private String user;

    @Override
    public String toString() {
        return title + ":" + href;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
