package cn.pcshao.util.pcsutil.utils;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiRobotSendRequest;
import com.dingtalk.api.response.OapiRobotSendResponse;
import com.taobao.api.ApiException;
import com.taobao.api.internal.toplink.embedded.websocket.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author pcshao.cn
 * @date 2021-03-22
 */
@Component
public class DingTalkUtil {

    private Logger logger = LoggerFactory.getLogger(DingTalkUtil.class);

    private String webhook;
    private String robotSec;
    private int queueCapacity = 5;
    /**
     * 消息队列
     */
    private Queue<Message> queue;
    /**
     * dingTalk手机号列表，配合消息队列使用，可被同一个线程重复添加，注意清空消息队列时remove
     */
    private ThreadLocal<List<String>> mobiles = new ThreadLocal<>();

    public DingTalkUtil(@Value("${dingtalk.webhook}") String webhook,
                        @Value("${dingtalk.robotSec}") String robotSec,
                        @Value("${dingtalk.queueCapacity}") String queueCapacity) {
        this.webhook = webhook;
        this.robotSec = robotSec;
        this.queueCapacity = Integer.parseInt(queueCapacity);
        this.queue = new LinkedBlockingQueue<>(this.queueCapacity);
    }

    /**
     * 钉钉发送消息体
     */
    public static class Message {
        private String message;
        /**
         * 需要at的手机号列表
         */
        private List<String> atMobiles = new ArrayList<>();
        private Boolean isAtAll = false;

        /**
         * 对单条消息增加at手机号
         * @param mobile
         * @return
         */
        public Message addAtMobile(String mobile) {
            if (mobile != null) {
                atMobiles.add(mobile);
            }
            return this;
        }

        public String getMessage() {
            return message;
        }

        public Message setMessage(String message) {
            this.message = message;
            return this;
        }

        public List<String> getAtMobiles() {
            return atMobiles;
        }

        public void setAtMobiles(List<String> atMobiles) {
            this.atMobiles = atMobiles;
        }

        public Boolean getAtAll() {
            return isAtAll;
        }

        public void setAtAll(Boolean atAll) {
            isAtAll = atAll;
        }
    }

    /**
     * 按队列发送消息
     *
     * @param message 消息
     * @param pour    推出队列中所有消息
     */
    public void sendMessageQueue(Message message, boolean pour) {
        queue.add(message);
        if (mobiles.get() == null)
            mobiles.set(new ArrayList<>());
        mobiles.get().addAll(message.getAtMobiles());
        // 队列满或者存在清空标识时，清空队列并推送消息
        if (queue.size() >= queueCapacity || pour) {
            StringBuilder sb = new StringBuilder();
            while (queue.size() > 0) {
                sb.append(queue.poll().getMessage()).append("\n");
            }
            sendMessage(sb.toString(), mobiles.get(), message.getAtAll());
            mobiles.remove();
        }
    }

    public void sendMessage(String message, List<String> atMobiles, boolean atAll) {
        OapiRobotSendRequest request = new OapiRobotSendRequest();
        request.setMsgtype("text");
        OapiRobotSendRequest.Text text = new OapiRobotSendRequest.Text();
        text.setContent(message);
        request.setText(text);

        OapiRobotSendRequest.At at = new OapiRobotSendRequest.At();
        at.setAtMobiles(atMobiles);
        at.setIsAtAll(atAll);
        request.setAt(at);
        try {
            Long timestamp = System.currentTimeMillis();
            String sign = generateSign(robotSec, timestamp);
            DingTalkClient client = new DefaultDingTalkClient(webhook + "&timestamp=" + timestamp + "&sign=" + sign);
            OapiRobotSendResponse response = client.execute(request);
            logger.info("发送成功！" + response);
        } catch (ApiException e) {
            logger.error("钉钉推送有误！", e);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    private String generateSign(String robotSec, Long timestamp) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        // 生成验签码,加签
        String stringToSign = timestamp + "\n" + robotSec;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(robotSec.getBytes("UTF-8"), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
        String sign = URLEncoder.encode(Base64.encodeToString(signData, false), "UTF-8");
        return sign;
    }
}