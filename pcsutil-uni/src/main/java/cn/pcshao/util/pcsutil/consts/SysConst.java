package cn.pcshao.util.pcsutil.consts;

/**
 * @author pcshao.cn
 * @date   2019-10-12
 * 系统常量类
 */
public class SysConst {
    public static final String ON =  "1";
    public static final String OFF =  "0";
}
