package cn.pcshao.util.pcsutil.consts;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pcshao.cn
 * @date 2022/12/5
 */
@Component
public class DingTalkMobileConst {

    public static Map<String, String> MOBILE_MAP;

    static {
        MOBILE_MAP = new HashMap<>();
    }

    static class AtEntity {
        private String name;
        private String mobile;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
