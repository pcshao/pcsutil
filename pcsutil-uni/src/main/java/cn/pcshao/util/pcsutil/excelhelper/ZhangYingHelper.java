package cn.pcshao.util.pcsutil.excelhelper;

import cn.pcshao.oa.common.util.ExcelUtil;
import cn.pcshao.oa.common.util.StringUtils;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ---------------------
 * 20230331 init
 * ---------------------
 * 环境要求：JDK 1.8+、JRE环境变量java
 * 开始：执行start.bat
 * 1. 读取本目录下“input.xls”（请先转换为97-2003格式excel 后缀xls）
 * 2. 读取excel的第一个sheet的第一二列的所有行
 * 3. 逐行扫描，将第N行至N+x行的第一列合并过的单元格，对应其第二列的所有文本拼接（换行分隔）
 * 4. 输出到本目录下的"output-时间戳.xls"
 *
 * @author pcshao.cn
 * @date 2023/3/31
 */
public class ZhangYingHelper {

    public static void main(String[] args) {
        List<Result> results = new ArrayList<>();
        try {
            String path = "input.xls";
            List<List> lists = readExcel(path);
            Result result = new Result();
            for (int i = 0; i < lists.size(); i++) {
                List list = lists.get(i);
                if (StringUtils.isNotBlank(list.get(0).toString())) {
                    results.add(result);
                    result = new Result();
                    result.setModNo(list.get(0).toString());
                    result.setContent(list.get(1).toString());
                    // 如果是最后一条，直接add返回
                    if (i == lists.size() - 1) {
                        results.add(result);
                    }
                } else {
                    result.setContent(result.getContent() + "\n" + list.get(1).toString());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //
        System.out.println(results);
        //
        String outputPath = "output-" + System.currentTimeMillis() + ".xls";
        try {
            write(outputPath, results);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    private static void write(String path, List<Result> results) throws IOException, WriteException {
        WritableWorkbook workbook = Workbook.createWorkbook(new File(path));
        WritableSheet sheet = workbook.createSheet("1", 0);
        for (int i = 0; i < results.size(); i++) {
            Result t = results.get(i);
            sheet.addCell(new Label(0, i, t.getModNo()));
            sheet.addCell(new Label(1, i, t.getContent()));
        }
        workbook.write();
        workbook.close();
    }

    static class Result {
        private String modNo;
        private String content;

        public String getModNo() {
            return modNo;
        }

        public void setModNo(String modNo) {
            this.modNo = modNo;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        @Override
        public String toString() {
            return modNo + "    " + content + " \n";
        }
    }

    private static List<List> readExcel(String path) throws FileNotFoundException {
        return ExcelUtil.TransExcelToList(new FileInputStream(path));
    }
}
