package cn.pcshao.util.pcsutil.creshelper;

import org.springframework.util.StringUtils;
import java.util.Objects;

public class CrTask {

    private final String COMMAND_1 = "cd /home/hundsun/src \n";
    private final String COMMAND_2 = "make -f ";
    private final String SO_PREX = "lib";
    private final String SO_SUFX = "flow.10.so";
    private final String remoteSrcPath = "/home/hundsun/src";  //远程路径
    private final String remoteSoPath = "/home/hundsun/appcom";  //远程路径
    private String type;  //类型 LS or AS
    private String localPath;  //本地路径 必须
    private String sendPath;   //递交路径 必须
    private String fileName;   //模块文件名 crdtcontract
    private String modelName;  //模块中文名 融资融券合同

    public String getSoName(){
        return SO_PREX+ this.getFileName()+ SO_SUFX;
    }

    public String getCommand() {
        return COMMAND_1+ COMMAND_2+ fileName+ "flow.gcc \n";
    }

    public void checkSelf() throws Exception {
        if (StringUtils.isEmpty(fileName) || StringUtils.isEmpty(modelName)){
            throw new Exception("模块文件名与模块中文名未赋值");
        }
        if (type.equals("ERROR"))
            throw new Exception("类型错误，既不是业务逻辑也不是原子");
    }

    public void setModelName(String modelName) {
        if (modelName.split("_")[0].equals("业务逻辑"))
            this.type = "LS";
        else if (modelName.split("_")[0].equals("原子"))
            this.type = "AS";
        else
            this.type = "ERROR";
        this.modelName = modelName;
    }

    public CrTask(String localPath, String sendPath) {
        this.localPath = localPath;
        this.sendPath = sendPath;
    }

    public String getType() {
        return type;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getRemoteSrcPath() {
        return remoteSrcPath;
    }

    public String getRemoteSoPath() {
        return remoteSoPath;
    }

    public String getModelName() {
        return modelName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSendPath() {
        return sendPath;
    }

    public void setSendPath(String sendPath) {
        this.sendPath = sendPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CrTask task = (CrTask) o;
        return Objects.equals(fileName, task.fileName) &&
                Objects.equals(modelName, task.modelName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, modelName);
    }
}
