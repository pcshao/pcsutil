package cn.pcshao.util.pcsutil.creshelper;

import cn.pcshao.util.pcsutil.MenuExecuteInterface;
import cn.pcshao.util.pcsutil.common.JSchExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author pcshao.cn
 * @date   2019-10-31
 * CRES 编译辅助
 *  1.仅支持指定目录下的一级目录，不支持非叶子节点模块
 */
@Service
public class CresHelper implements MenuExecuteInterface {
    private static Logger log = LoggerFactory.getLogger(CresHelper.class);

    @Resource
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    private Queue<CrTask> taskQueue;
    private int QUEUE_LENGTH = 2;
    private String USERNAME;
    private String PASSWORD;
    private String HOST;
    private int PORT;
    private String CHARSET;
    private String LOCAL_PATH;
    private String SEND_PATH;
    private boolean SEND_WITH_FILE;
    private boolean AUTO_RESTART_NODE;
    private final String[] LOCAL_LS_SUFX = {"flow.gcc", "flow.cpp", "func.h", "func.cpp"};
    private final String[] LOCAL_AS_SUFX = {"flow.gcc", "flow.pc", "func.h", "func.pc"};
    private final String BACKUP_FILE_SUFX = ".bak";

    private SqlFileFilter sqlFileFilter;

    public CresHelper(@Value("${creshelper.username}") String username,
                      @Value("${creshelper.password}") String password,
                      @Value("${creshelper.host}") String host,
                      @Value("${creshelper.port}") String port,
                      @Value("${creshelper.charset}") String charset,
                      @Value("${creshelper.queuelength}") String queueLength,
                      @Value("${creshelper.localpath}") String localpath,
                      @Value("${creshelper.sendpath}") String sendpath,
                      @Value("${creshelper.sendwithsrc}") boolean sendwithsrc,
                      @Value("${creshelper.restart_node}") boolean restart_node
                      ){
        this.USERNAME = username;
        this.PASSWORD = password;
        this.HOST = host;
        this.PORT = Integer.parseInt(port);
        this.CHARSET = charset;
        this.QUEUE_LENGTH = Integer.parseInt(queueLength);
        this.LOCAL_PATH = localpath;
        this.SEND_PATH = sendpath;
        this.SEND_WITH_FILE = sendwithsrc;
        this.AUTO_RESTART_NODE = restart_node;
        this.sqlFileFilter = new SqlFileFilter();
    }

    /**
     * 主入口
     */
    public void execute(){
        //实例化文件任务队列
        taskQueue = new LinkedBlockingQueue<>();
        //开启线程实时取任务队列后消费
        taskExecutor.execute(()->{
            log.info("扫描中: "+ LOCAL_PATH);
            while (true) {
                if (taskQueue.size() != 0) {
                    CrTask task = taskQueue.peek();
                    try {
                        task.checkSelf();
                    } catch (Exception e) {
                        log.error("错误生成任务队列：" + e.getMessage());
                        continue;
                    }
                    //删除.mvc文件作为触发标志
                    File sourceFile = new File(task.getLocalPath() + "/" + task.getModelName() + "/" + task.getFileName() + "flow.mvc");
                    File backupFile = new File(task.getLocalPath() + "/" + task.getModelName() + "/" + task.getFileName() + "flow"+ BACKUP_FILE_SUFX);
                    //重复任务队列，检查.mvc文件是否还存在，否则
                    if (!sourceFile.exists()) {  //过滤 删除.mvc文件后触发的一次文件状态变更task
                        taskQueue.remove(task);
                        log.debug("sourceFile不存在，任务作废："+ task.getModelName());
                        continue;
                    }else {
                        log.debug("开始备份.mvc文件并删除原文件");
                        try {
                            FileCopyUtils.copy(sourceFile, backupFile);
                        } catch (IOException e) {
                            log.debug("MVC文件IO错误", e);
                        } finally {
                            sourceFile.delete();
                        }
                        log.debug("备份.mvc文件成功，作为触发标志将在下一次生成代码后驱动程序");
                    }
                    //从任务队列中取出task开启新线程执行
                    taskQueue.poll();
                    taskExecutor.execute(()->{
                        execute(task);
                        Thread.interrupted();
                    });
                }
                try {
                    Thread.sleep(1000*3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        //主线程文件监控产生任务队列
        try {
            CrTask task = new CrTask(LOCAL_PATH, SEND_PATH);
            fileMonitor(task);  //文件监控作为主线程 while循环
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取变动文件
     *  while死循环，注意新开线程支持此方法
     * @throws IOException
     * @throws InterruptedException
     */
    private void fileMonitor(CrTask crTask) throws IOException, InterruptedException {
        String basePath = crTask.getLocalPath();
        //文件监听服务
        WatchService watchService = null;
        watchService = FileSystems.getDefault().newWatchService();
        //给文件路径注册监听服务
        Paths.get(basePath)
                .register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_CREATE);
        while (true){
            WatchKey watchKey = watchService.take();
            List<WatchEvent<?>> watchEvents = watchKey.pollEvents();
            for (WatchEvent event : watchEvents){
                if (StandardWatchEventKinds.ENTRY_MODIFY == event.kind() || StandardWatchEventKinds.ENTRY_CREATE == event.kind()){
                    String modelName = event.context().toString();
                    if (modelName.split("\\.").length == 1) {  //排除掉有后缀名的
                        log.info("检测到代码生成目录变化: "+ modelName);
                        //等待文件状态完好，送入 c://基础路径/模块名
                        String fullName = waitFileComplete(basePath+ "/"+ modelName);
                        if (fullName.length() < 8)
                            break;
                        //文件名：文件全称去掉flow.gcc 模块名：modelName
                        crTask.setFileName(fullName.substring(0, fullName.length()-8));
                        crTask.setModelName(modelName);
                        //task对象重写equals方法，只判断fileName与modelName
                        if (!taskQueue.contains(crTask))
                            taskQueue.add(crTask);
                    }
                    break;
                }
            }
            watchKey.reset();
            //稍作休眠
            Thread.sleep(1000*3);
        }
    }

    /**
     * 等待代码生成
     * @param path  模块完整路径
     * @throws InterruptedException
     */
    private String waitFileComplete(String path) throws InterruptedException {
        //等待文件稍作生成，但仍然支持文件0KB时读取状态
        Thread.sleep(1000*1);
        String fullName = "";
        File file = new File(path);
        File[] files = file.listFiles();
        File tarF = null;
        while (true) {
            for (File f : files) {
                if (f.getName().length() > 3) {
                    String temp = f.getName().substring(f.getName().length() - 3);
                    if (temp.equals("cpp") || temp.equals("gcc") || temp.equals(".pc")) {  //只扫描pc、gcc文件
                        long space = f.length() / 1024;  //正在生成的代码是会短暂变成0KB
                        if (space == 0)
                            tarF = f;
                        if (temp.equals("gcc"))  //找到.gcc文件
                            fullName = f.getName();
                    }
                }
            }
            if (tarF == null || tarF.length() > 0)
                break;
            log.info(path+ "/"+ tarF.getName()+ "文件大小为0，等待其生成完毕...");
            Thread.sleep(5000);  //休眠会再扫描，防止资源占用
        }
        log.info("校验代码生成完整，开始提交任务队列");
        return fullName;
    }

    /**
     * 上传编译下载
     */
    public void execute(CrTask crTask) {
        JSchExecutor jSchExecutor = new JSchExecutor(USERNAME, PASSWORD, HOST, PORT, CHARSET);
        try {
            jSchExecutor.connect();
            //传至LINUX目录
            log.info("正在上传: "+ crTask.getFileName());
            if ("LS".equals(crTask.getType())) {
                for (int i = 0; i < LOCAL_LS_SUFX.length; i++) {
                    String local = crTask.getLocalPath()+ "/"+ crTask.getModelName()+ "/"+ crTask.getFileName() + LOCAL_LS_SUFX[i];
                    String remote = crTask.getRemoteSrcPath()+ "/"+ crTask.getFileName()+ LOCAL_LS_SUFX[i];
                    String send = crTask.getSendPath()+ "/"+ crTask.getFileName() + LOCAL_LS_SUFX[i];
                    jSchExecutor.uploadFile(local, remote);
                    //拷贝源码至SO同目录
                    if (SEND_WITH_FILE)
                        FileCopyUtils.copy(new File(local), new File(send));
                }
                log.info("上传LS成功: "+ crTask.getFileName());
            }
            else if ("AS".equals(crTask.getType())) {
                for (int i = 0; i < LOCAL_AS_SUFX.length; i++) {
                    String send = crTask.getSendPath()+ "/"+ crTask.getFileName() + LOCAL_AS_SUFX[i];
                    String local = crTask.getLocalPath()+ "/"+ crTask.getModelName()+ "/"+ crTask.getFileName() + LOCAL_AS_SUFX[i];
                    String remote = crTask.getRemoteSrcPath()+ "/"+ crTask.getFileName()+ LOCAL_AS_SUFX[i];
                    //拷贝源码至SO同目录
                    if (SEND_WITH_FILE)
                        FileCopyUtils.copy(new File(local), new File(send));
                    jSchExecutor.uploadFile(local, remote);
                }
                log.info("上传AS成功: "+ crTask.getFileName());
                //拷贝本地sql
                if (SEND_WITH_FILE) {
                    File file = new File(crTask.getLocalPath() + "/" + crTask.getModelName() + "/");
                    File[] files = file.listFiles(sqlFileFilter);
                    for (int i = 0; i < files.length; i++) {
                        String send = crTask.getSendPath() + "/" + files[i].getName();
                        FileCopyUtils.copy(files[i], new File(send));
                    }
                }
            }else
                log.error("上传源码文件失败");
            //编译和下载SO文件
            int returnCode = jSchExecutor.makeFile(crTask.getCommand(), crTask.getFileName());
            if (0 == returnCode){
                String targetFileName = crTask.getSoName();
                jSchExecutor.downloadFile(crTask.getRemoteSoPath()+ "/"+ targetFileName, crTask.getSendPath()+ "/"+ targetFileName);
                log.info("下载SO文件成功: "+ targetFileName);
                if (AUTO_RESTART_NODE){
                    //TODO
                    jSchExecutor.execCmd();
                }
            }else if (1 == returnCode){
                log.info("任务失败");
            }
        } catch (Exception e) {
            log.info("远程服务器出错！", e);
            e.printStackTrace();
        } finally {
            jSchExecutor.disconnect();
            log.info("断开连接"+ HOST);
        }
    }

}
class SqlFileFilter implements FileFilter {
    @Override
    public boolean accept(File file) {
        if(file.isDirectory())
            return false;
        else {
            String name = file.getName();
            if(name.endsWith(".sql"))
                return true;
            else
                return false;
        }
    }
}
