package cn.pcshao.util.pcsutil;

import cn.pcshao.util.pcsutil.cms.CmsDevopsHelper;
import cn.pcshao.util.pcsutil.creshelper.CresHelper;
import cn.pcshao.util.pcsutil.spider.HsHomeSpider;
import cn.pcshao.util.pcsutil.sqldup.NBsqlPatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * @author pcshao.cn
 * @date   2019-10-11
 * 入口
 */
@Component
public class Menu {

    public Menu(@Autowired() NBsqlPatch nBsqlPatch,
                @Autowired() CresHelper cresHelper,
                @Autowired() HsHomeSpider hsHomeSpider,
                @Autowired() CmsDevopsHelper cmsDevopsHelper){
        //command console
        notice();
        while (true){
            Scanner scanner = new Scanner(System.in);
            String inputCommand = scanner.next();
            if ("0".equals(inputCommand))
                System.exit(1);
            if ("1".equals(inputCommand))
                nBsqlPatch.execute();
            if ("2".equals(inputCommand))
                cresHelper.execute();
            if ("3".equals(inputCommand))
                hsHomeSpider.execute();
            if ("4".equals(inputCommand))
                cmsDevopsHelper.execute();

        }
    }

    public static void notice(){
        System.out.println("输入1-脚本整理 2-自动上传编译取包 3-论坛爬虫 4-天工 0-退出");
    }
}
