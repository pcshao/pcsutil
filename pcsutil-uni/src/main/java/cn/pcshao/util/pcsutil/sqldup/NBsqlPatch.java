package cn.pcshao.util.pcsutil.sqldup;

import cn.pcshao.util.pcsutil.Menu;
import cn.pcshao.util.pcsutil.MenuExecuteInterface;
import cn.pcshao.util.pcsutil.consts.SysConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.*;

/**
 * @author pcshao.cn
 * @date   2019-10-11
 * 历史脚本清理
 */
@Component
public class NBsqlPatch implements MenuExecuteInterface {

    private Logger logger = LoggerFactory.getLogger(NBsqlPatch.class);

    private String FILE_PATH;  //文件路径
    private String ENCODE;    //文件编码（常量只在构造方法中用，别的封装的函数追随入参唯一原则）
    private String overNum;
    private String exceptConfigNoStr;
    private String createFileSwitch;  //是否输出注释后的文件 1-是 0-否
    private String possibilityMin;  //大于可能性
    private String possibilityMax;  //小于可能性
    private File file;

    public NBsqlPatch(@Value("${sqlhelper.filepath}") String filepath,
                      @Value("${sqlhelper.encode}") String encode,
                      @Value("${sqlhelper.output.overnum}") String overNum,
                      @Value("${sqlhelper.output.except_config_no}") String exceptConfigNoStr,
                      @Value("${sqlhelper.output.possibility_min}") String possibilityMin,
                      @Value("${sqlhelper.output.possibility_max}") String possibilityMax,
                      @Value("${sqlhelper.output.creatfile_switch}") String createFileSwitch){
        this.FILE_PATH = filepath;
        this.ENCODE = encode;
        this.overNum = overNum;
        this.exceptConfigNoStr = exceptConfigNoStr;
        this.possibilityMin = possibilityMin;
        this.possibilityMax = possibilityMax;
        this.createFileSwitch = createFileSwitch;
        this.file = new File(filepath);
    }

    public void execute(){
        try {
            execute(file);
        } catch (FileNotFoundException e) {
            logger.error("文件路径不存在："+ FILE_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void execute(File file) throws IOException {
        logger.info("文件加载中...");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), ENCODE));

        String line;
        int line_no = 1;
        int config_no;
        Unit unit;
        List<Unit> unitList = new ArrayList();
        //循环读取每一行中含有 update xxx config_no = 31317;
        while ((line = br.readLine()) != null){
            boolean isContains = false;
            List<Integer> hisLineList = new ArrayList<>();
            List<Integer> hasLineList = new ArrayList<>();
            if ((config_no = getConfigNoFromLine(line)) != -1){
                unit = new Unit(config_no, line_no, line);
                unit.setNum(1);  //出现第一次
                hisLineList.add(line_no);
                unit.setHisLineNoList(hisLineList);  //历史出现行
                if (line.trim().startsWith("--")) {
                    hasLineList.add(line_no);
                    unit.setHasLineNoList(hasLineList);  //历史注释行
                }
                for (int i = 0; i < unitList.size(); i++) {
                    if (unitList.get(i).equals(unit)){
                        unit.setNum(unitList.get(i).getNum()+1);  //更新出现第(N+1)次
                        List<Integer> oldLineList = unitList.get(i).getHisLineNoList();
                        oldLineList.add(line_no);
                        unit.setHisLineNoList(oldLineList);  //更新记录曾出现过的行数
                        List<Integer> oldHasLineList = unitList.get(i).getHasLineNoList();
                        if (oldHasLineList == null)
                            oldHasLineList = hasLineList;
                        if (line.trim().startsWith("--")) {
                            if (unit.getHasLineNoList() != null) {
                                oldHasLineList.addAll(unit.getHasLineNoList());  //也可以写成add(line)
                            }
                        }
                        unit.setHasLineNoList(oldHasLineList);  //更新被注释过的行数
                        unit.calculateAnnotationScale();
                        unitList.set(i, unit);
                        isContains = true;
                        break;
                    }
                }
                if (!isContains) {
                    unitList.add(unit);
                }
            }
            line_no ++;
        }
        //输出到控制台和文件
        File outputFile = new File("resultFile.txt");
        printList(unitList, outputFile, Integer.parseInt(overNum)
                , Float.parseFloat(possibilityMin), Float.parseFloat(possibilityMax)
                , exceptConfigNoStr);
        //关闭流
        br.close();
        //输出副本
        if (SysConst.ON.equals(createFileSwitch)) {
            createFile(FILE_PATH, FILE_PATH);
        }
        logger.info("程序执行完毕!");
        Menu.notice();
    }

    //输出注释后的文件
    private void createFile(String inPath, String outPath) throws IOException {
        logger.info("输出注释后的文件中...");
        File file_in = new File(inPath);
        File file_out = new File(outPath + "out.sql");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file_in), ENCODE));
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_out), ENCODE));

        String text;
        while ((text = br.readLine()) != null){
            //什么时候哪些地方加上注释
            pw.println(text);
        }
        //关闭流
        pw.close();
        br.close();
        logger.info("输出注释后的文件成功!");
    }

    //从行中获取config_no 截取 update xxx config_no = 31317;
    private int getConfigNoFromLine(String line) {
        String[] strings = line.split("where");
        if (strings.length >= 2 && strings[0].contains("update") && strings[1].length()<20){
            return Integer.parseInt(strings[1].split(";")[0].split("=")[1].trim());
        }else {
            return -1;
        }
    }

    //输出
    private void printList(List<Unit> unitList, File file_out, int overNum,float possibilityMin, float possibilityMax, String exceptConfigNoStr) throws FileNotFoundException, UnsupportedEncodingException {
        logger.info("即将打印出现次数大于{}次,注释过可能性在({}%, {}%)的数据", overNum, possibilityMin, possibilityMax);
        int n = 1;
        String[] strings = null;
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file_out), ENCODE));
        if (!StringUtils.isEmpty(exceptConfigNoStr)){
            logger.info("且过滤如下数据：[{}]"+ exceptConfigNoStr);
            strings = exceptConfigNoStr.split("\t");
        }
        for (int i = 0; i < unitList.size(); i++) {
            //排除出现次数大于overNum的
            if (unitList.get(i).getNum() <= overNum)
                continue;
            //筛选注释过可能性
            if (unitList.get(i).getAnnotationScale()*100 < possibilityMin
                    || unitList.get(i).getAnnotationScale()*100 > possibilityMax)
                continue;
            //排除指定config_no
            boolean exceptTarget = false;
            if (strings != null){
                for (int j = 0; j < strings.length; j++) {
                    if (unitList.get(i).getConfig_no() == Integer.parseInt(strings[j].trim())){
                        exceptTarget = true;
                    }
                }
            }
            if (exceptTarget)
                continue;
            //输出日志
            logger.info("NO."+ n+ unitList.get(i).toString());
            //输出文件
            pw.println("NO."+ n+ unitList.get(i).toString());
            n++;
        }
        logger.info("结果集文件已输出到工作目录下："+ file_out.getAbsolutePath());
        //关闭流
        pw.close();
    }

}
class Unit {
    private int config_no;
    private int line_no;
    private int num;
    private List<Integer> hisLineNoList;
    private List<Integer> hasLineNoList;
    private String line_str;
    private float annotationScale;

    public Unit(int config_no, int line_no, String line_str) {
        this.config_no = config_no;
        this.line_no = line_no;
        this.line_str = line_str;
    }

    public int getConfig_no() {
        return config_no;
    }

    public int getLine_no() {
        return line_no;
    }

    public String getLine_str() {
        return line_str;
    }

    public void setLine_str(String line_str) {
        this.line_str = line_str;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public List<Integer> getHisLineNoList() {
        return hisLineNoList;
    }

    public void setHisLineNoList(List<Integer> hisLineNoList) {
        this.hisLineNoList = hisLineNoList;
    }

    public List<Integer> getHasLineNoList() {
        return hasLineNoList;
    }

    public void setHasLineNoList(List<Integer> hasLineNoList) {
        this.hasLineNoList = hasLineNoList;
    }

    public float getAnnotationScale() {
        return annotationScale;
    }

    public void setAnnotationScale(float annotationScale) {
        this.annotationScale = annotationScale;
    }

    @Override
    public String toString() {
        return "{ " +
                "config_no=" + config_no +
                ", 最后行数=" + line_no +
                ", 出现次数=" + num +
                ", 注释过可能性=" + annotationScale*100 + "%" +
                ", 历史行数=" + hisLineNoList +
                ", 已注释行=" + hasLineNoList +
//                ", \t源='" + line_str.trim() + "'" +
                '}';
    }

    public String out2File(){
        return "{ " +
                "config_no=" + config_no +
                ", 最后行数=" + line_no +
                ", 出现次数=" + num +
                ", 可能已全部注释(如果最后行数连续)=" + ((hisLineNoList==null?0:hisLineNoList.size())-1 <= (hasLineNoList==null?0:hasLineNoList.size()) ? "是":"否")+
                ", 历史行数=" + hisLineNoList +
                ", 已注释行=" + hasLineNoList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return config_no == unit.config_no;
    }

    public void calculateAnnotationScale() {
        float poss = 0;
        if (hisLineNoList != null && hasLineNoList != null){
            //1. 历史出现行-历史注释行 == 1 为最大可能已注释
            float w1 = hisLineNoList.size()-hasLineNoList.size();
            poss += 1 / (w1==0 ? 1:w1);
            //2. 历史出现行最后几次出现挨得比较近(目前两次，计算rowNum后的两条update) 可能已注释的权重++
            if ((hisLineNoList.size() == 2)){
                float w2 = hisLineNoList.get(1) - hisLineNoList.get(0);
                poss += 1 / (w2);
            }
            //不超过1
            poss = poss > 1 ? 1:poss;
            this.annotationScale = poss;
        }
    }
}
