package cn.pcshao.util.pcsutil.cms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

import cn.pcshao.oa.common.util.ExcelUtil;
import cn.pcshao.oa.common.util.StringUtils;
import cn.pcshao.oa.common.util.YamlAndPropertySourceFactory;
import cn.pcshao.util.pcsutil.MenuExecuteInterface;
import cn.pcshao.util.pcsutil.consts.DingTalkMobileConst;
import cn.pcshao.util.pcsutil.utils.DingTalkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

/**
 * @author pcshao.cn
 * @date 2022/12/2
 */
@Service
@PropertySource(value = "classpath:custom.yml", factory = YamlAndPropertySourceFactory.class)
public class CmsDevopsHelper implements MenuExecuteInterface {

    private static Logger logger = LoggerFactory.getLogger(CmsDevopsHelper.class);

    @Resource
    DingTalkUtil dingTalkUtil;

    /**
     * 目标excel路径，仅支持97-2003.xls
     */
    private String path;
    private String prexStr = "开发待排查缺陷列表：@";
    private String endStr = "排查后请及时更新至在线excel~";

    public CmsDevopsHelper(@Value("${cmsdevops.path}") String path,
                           @Value("${cmsdevops.prex-str}") String prexStr,
                           @Value("${cmsdevops.end-str}") String endStr) {
        this.path = path;
        if (StringUtils.isNotBlank(prexStr))
            this.prexStr = prexStr;
        if (StringUtils.isNotBlank(endStr))
            this.endStr = endStr;
    }

    public void execute() {
        // 读取excel
        List<List> lists = null;
        try {
            lists = readFromExcel(path);
        } catch (FileNotFoundException e) {
            logger.error("文件不存在{}", path, e);
            return;
        }
        // 生成待发送信息列表
        lists.remove(0);
        List<ExcelUnit> excelUnits = transToUnit(lists);
        Map<String, List<ExcelUnit>> toSendList = genToSendList(excelUnits);
        // dingTalk
        int result = send(toSendList);
        // exit
        System.exit(result);
    }

    /**
     * 发送
     *
     * @param stringListMap
     */
    private int send(Map<String, List<ExcelUnit>> stringListMap) {
        if (CollectionUtils.isEmpty(stringListMap))
            return 0;
        for (Map.Entry<String, List<ExcelUnit>> item : stringListMap.entrySet()) {
            // 拼接前缀，联系人翻译成手机号用于at点亮
            String realMobile = transMobile(item.getKey());
            sendMessageQueue(prexStr + realMobile, false, realMobile);
            List<ExcelUnit> value = item.getValue();
            StringBuilder sb = new StringBuilder();
            for (ExcelUnit itemInner : value) {
                sb.append(genDingMessage(itemInner));
            }
            sendMessageQueue(sb.toString(), false, null);
        }
        sendMessageQueue(endStr, true, null);
        return 1;
    }

    private String transMobile(String key) {
        return Optional.ofNullable(DingTalkMobileConst.MOBILE_MAP.get(key)).orElse(key);
    }

    /**
     * 发送消息队列
     *
     * @param message   消息
     * @param pour  是否清空消息队列
     * @param mobile    该条消息需要at的手机号
     */
    private void sendMessageQueue(String message, boolean pour, String mobile) {
        logger.debug(message);
        dingTalkUtil.sendMessageQueue(new DingTalkUtil.Message().setMessage(message).addAtMobile(mobile), pour);
    }

    private String genDingMessage(ExcelUnit itemInner) {
        if (itemInner.isForcePush())
            return String.format("#%d-%s（有了新回复） ", itemInner.getId(), itemInner.getLevel());
        else
            return String.format("#%d-%s ", itemInner.getId(), itemInner.getLevel());
    }

    /**
     * 生成待发送列表
     *
     * @param excelUnits excel单元集合
     * @return map<developer, 归属的item>
     */
    private Map<String, List<ExcelUnit>> genToSendList(List<ExcelUnit> excelUnits) {
        HashMap<String, List<ExcelUnit>> toSendList = new HashMap<>();
        for (ExcelUnit item : excelUnits) {
            // 匹配开发不为空且开发备注为空的
            boolean match = StringUtils.isNotBlank(item.getDeveloper()) && !item.getDeveloper().trim().equals("0")
                    && (StringUtils.isBlank(item.getDeveloperDesc()) || item.getDeveloperDesc().trim().equals("0"));
            boolean forcePush = "1".equals(item.getForcePush());
            if (match || forcePush) {
                List<ExcelUnit> temp = toSendList.get(item.getDeveloper());
                if (temp == null) {
                    toSendList.put(item.getDeveloper(), new ArrayList<>());
                    temp = toSendList.get(item.getDeveloper());
                }
                temp.add(item);
            }
        }
        return toSendList;
    }

    /**
     * 在此定义excel与对象的映射
     *
     * @param lists
     * @return
     */
    private List<ExcelUnit> transToUnit(List<List> lists) {
        return lists.stream().map(item -> {
            ExcelUnit excelUnit = new ExcelUnit(item.get(6).toString(), item.get(8).toString());
            try {
                excelUnit.setId(Integer.parseInt(item.get(0).toString()));
            } catch (Exception e) {
                excelUnit.setId(0);
            }
            // 标题
            excelUnit.setTitle(item.get(1).toString());
            // 描述
            excelUnit.setDesc(item.get(2).toString());
            // 状态
            excelUnit.setStatus(item.get(3).toString());
            // 处理人
            excelUnit.setDealPerson(item.get(4).toString());
            // 所属项目
            excelUnit.setBelonging(item.get(5).toString());
            // 缺陷等级
            excelUnit.setLevel(item.get(6).toString());
            // 开发
            excelUnit.setDeveloper(item.get(7).toString());
            // 标签
            // excelUnit.setForcePush(item.get(7).toString());
            // 开发备注
            excelUnit.setDeveloperDesc(item.get(9).toString());
            // 强推标识
            excelUnit.setForcePush(item.get(10).toString());
            return excelUnit;
        }).collect(Collectors.toList());
    }

    private List<List> readFromExcel(String path) throws FileNotFoundException {
        try {
            return ExcelUtil.TransExcelToList(new FileInputStream(new File(path)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        }
    }

    static class ExcelUnit {
        private Integer id;
        private String title;
        private String desc;
        private String status;
        private String dealPerson;
        private String level;
        private String forcePush;
        private String developer;
        private String developerDesc;
        private String belonging;

        public ExcelUnit(String developer, String developerDesc) {
            this.developer = developer;
            this.developerDesc = developerDesc;
        }

        /**
         * 是否强推
         * @return
         */
        public boolean isForcePush() {
            return "1".equals(this.forcePush);
        }

        public String getBelonging() {
            return belonging;
        }

        public void setBelonging(String belonging) {
            this.belonging = belonging;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDealPerson() {
            return dealPerson;
        }

        public void setDealPerson(String dealPerson) {
            this.dealPerson = dealPerson;
        }

        public String getDeveloper() {
            return developer;
        }

        public void setDeveloper(String developer) {
            this.developer = developer;
        }

        public String getDeveloperDesc() {
            return developerDesc;
        }

        public void setDeveloperDesc(String developerDesc) {
            this.developerDesc = developerDesc;
        }

        public String getForcePush() {
            return forcePush;
        }

        public void setForcePush(String forcePush) {
            this.forcePush = forcePush;
        }
    }
}
