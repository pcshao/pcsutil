#20191011 V1.0 盘古开天辟地-脚本整理工具
#20191014 V1.1 支持 脚本整理工具-注释过可能性输出
#20191101 V1.2 支持 CRES自动上传编译取包工具
#20210321 V1.3 支持二手论坛爬虫
#20211108 V1.3.1 二手论坛爬虫支持redis统一缓存已发送的消息集合
#20221203 V1.4 支持 CMS辅助，excel解析机器人
#20230303 V1.5 模块拆分，新增ssh-helper（剥离核心jar，可交付客户）
1.准备
1.1 JDK1.8+
1.2 配置文件在resource目录下 xxx-xx.properties，比如application-26.properties。建议一台机器一份配置文件，这样在多台机器切换编译的时候只要切换配置文件接可以了，比如执行 java -jar pcsutil-1.2-exec.jar --spring.profile.active=26

2.执行
2.1 按照配置文件中说明配置使用
2.2 使用powershell或其他工具在当前目录执行 java -jar pcsutil-x.x-exec.jar --spring.profile.active=xx
2.3 遵循控制台菜单操作使用

3.关于
3.1 无

# 参考启动命令
cd C:\xxx\; java -jar pcsutil-1.3-exec.jar --spring.profiles.active=26
