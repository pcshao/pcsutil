package cn.pcshao.util.pcsutil.common;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pcshao.cn
 * @date   2019-10-31
 * Shell执行器
 */
public class JSchExecutor {
    private static Logger log = LoggerFactory.getLogger(JSchExecutor.class);

    private String USERNAME;
    private String PASSWORD;
    private String HOST;
    private int PORT = 22;
    private String CHARSET = "UTF-8";

    private JSch jsch;
    private Session session;
    private ChannelSftp sftp;

    public JSchExecutor(String username, String password, String host ) {
        this.USERNAME = username;
        this.PASSWORD = password;
        this.HOST = host;
    }

    public JSchExecutor(String username, String password, String host, int port) {
        this.USERNAME = username;
        this.PASSWORD = password;
        this.HOST = host;
        this.PORT = port;
    }

    public JSchExecutor(String username, String password, String host, int port, String charset) {
        this.USERNAME = username;
        this.PASSWORD = password;
        this.HOST = host;
        this.PORT = port;
        this.CHARSET = charset;
    }


    /**
     * 连接到指定的IP
     * @throws JSchException
     */
    public void connect() throws JSchException {
        jsch = new JSch();
        session = jsch.getSession(USERNAME, HOST, PORT);
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setPassword(PASSWORD);
        session.setConfig(config);
        session.connect();
        Channel channel = session.openChannel("sftp");
        channel.connect();
        sftp = (ChannelSftp) channel;
        log.info("连接到SFTP成功 host:" + HOST);
    }
    /**
     * 关闭连接
     */
    public void disconnect(){
        if (sftp != null && sftp.isConnected()) {
            sftp.disconnect();
        }
        if(session != null && session.isConnected()){
            session.disconnect();
        }
    }

    /**
     * 编译
     * @param command
     */
    public int makeFile(String command, String fileName) {
        log.debug("开始执行: "+ command);
        int returnCode  = -1;
        ChannelShell channel = null;
        BufferedReader reader = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {//open shell channel
            channel = (ChannelShell) session.openChannel("shell");  //shell命令需要\n结尾
            channel.connect(5000);
            log.info("连接到shell成功");
            //get stream
            inputStream = channel.getInputStream();
            outputStream = channel.getOutputStream();
            //write to stream
            outputStream.write(command.getBytes());
            outputStream.flush();
            log.info("正在编译: "+ fileName);
            //read from stream
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String temp;
            boolean specialTarget = false;
            while ((temp = reader.readLine()) != null){
                //TODO 多个任务执行时，貌似后面任务的输出流挡住了前面的，导致前面任务检测不到编译完成
                System.out.println(temp);
                //适配新环境
                if (temp.contains("warnings being treated as errors"))
                    specialTarget = true;
                if (temp.contains(fileName+ "flow.10.so") || specialTarget){
                    log.info("编译成功: "+ fileName);
                    returnCode = 0;
                    break;
                }
                if (temp.contains("错误")){
                    log.info("编译失败");
                    returnCode = 2;
                    break;
                }
            }
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(1000*2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (outputStream!=null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader!=null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (channel.isClosed()) {
                returnCode = channel.getExitStatus();
            }
            channel.disconnect();
        }
        return returnCode;
    }
    /**
     * 执行一条命令
     */
    public int execCmd(String command) throws Exception{
        log.debug("开始执行: " + command);
        int returnCode  = -1;
        ChannelExec channel = null;
        BufferedReader reader = null;
        //open channel
        channel = (ChannelExec) session.openChannel("exec");
        channel.setCommand(command);
        channel.setInputStream(null);
        channel.setErrStream(System.err);
        //inputStream channel
        InputStream in = channel.getInputStream();
        reader = new BufferedReader(new InputStreamReader(in));//charset problem
        //connect channel
        channel.connect();
        log.debug("The remote command is: " + command);
        String buf ;
        while ((buf = reader.readLine()) != null) {
            log.info(buf);
        }
        reader.close();
        // Get the return code only after the channel is closed.
        if (channel.isClosed()) {
            returnCode = channel.getExitStatus();
        }
        log.info("Exit-status:" + returnCode);
        //close channel
        channel.disconnect();
        return returnCode;
    }

    /**
     * 执行相关的命令
     */
    public void execCmd() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String command = "";
        BufferedReader reader = null;
        Channel channel = null;
        try {
            while ((command = br.readLine()) != null) {
                //open channel
                channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(command);
                channel.setInputStream(null);
                ((ChannelExec) channel).setErrStream(System.err);
                //connect channel
                channel.connect();
                InputStream in = channel.getInputStream();
                reader = new BufferedReader(new InputStreamReader(in,
                        Charset.forName(CHARSET)));
                String buf = null;
                while ((buf = reader.readLine()) != null) {
                    System.out.println(buf);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            channel.disconnect();
        }
    }

    /**
     * 上传文件
     */
    public void uploadFile(String local,String remote) throws Exception {
        File file = new File(local);
        if (file.isDirectory()) {
            throw new RuntimeException(local + "  is not a file");
        }
        InputStream inputStream = null;
        try {
            String path = remote.substring(0, remote.lastIndexOf("/")+ 1);
            if (!isDirExist(path)){
                createDir(path);
            }
            inputStream = new FileInputStream(file);
            //sftp
            sftp.setInputStream(inputStream);
            sftp.put(inputStream, remote);
        } catch (Exception e) {
            throw e;
        }finally{
            if(inputStream != null){
                inputStream.close();
            }
        }
    }
    /**
     * 下载文件
     */
    public void downloadFile(String remote,String local) throws Exception{
        OutputStream outputStream = null;
        try {
            if (!sftp.isConnected())
                sftp.connect(5000);
            outputStream = new FileOutputStream(new File(local));
            sftp.get(remote, outputStream);
            outputStream.flush();
        } catch (Exception e) {
            throw e;
        } finally{
            if(outputStream != null){
                outputStream.close();
            }
        }
    }

    /**
     * 移动到相应的目录下
     * @param pathName 要移动的目录
     * @return
     */
    public boolean changeDir(String pathName){
        if(pathName == null || pathName.trim().equals("")){
            log.info("切换无效路径名: "+ pathName);
            return false;
        }
        try {
            sftp.cd(pathName.replaceAll("\\\\", "/"));
            log.debug("directory successfully changed,current dir=" + sftp.pwd());
            return true;
        } catch (SftpException e) {
            log.error("failed to change directory",e);
            return false;
        }
    }

    /**
     * 创建一个文件目录，mkdir每次只能创建一个文件目录，这里用循环
     * 或者可以使用命令mkdir -p 来创建多个文件目录
     */
    public void createDir(String createPath) {
        try {
            if (isDirExist(createPath)) {
                sftp.cd(createPath);
                return;
            }
            String pathArray[] = createPath.split("/");
            StringBuffer filePath = new StringBuffer("/");
            for (String path : pathArray) {
                if (path.equals("")) {
                    continue;
                }
                filePath.append(path + "/");
                if (isDirExist(filePath.toString())) {
                    sftp.cd(filePath.toString());
                } else {
                    // 建立目录
                    sftp.mkdir(filePath.toString());
                    // 进入并设置为当前目录
                    sftp.cd(filePath.toString());
                }
            }
            sftp.cd(createPath);
        } catch (SftpException e) {
            throw new RuntimeException("创建路径错误：" + createPath);
        }
    }

    /**
     * 判断目录是否存在
     * @param directory
     * @return
     */
    public boolean isDirExist(String directory)
    {
        boolean isDirExistFlag = false;
        try
        {
            SftpATTRS sftpATTRS = sftp.lstat(directory);
            isDirExistFlag = true;
            return sftpATTRS.isDir();
        }
        catch (Exception e)
        {
            if (e.getMessage().toLowerCase().equals("no such file"))
            {
                isDirExistFlag = false;
            }
        }
        return isDirExistFlag;
    }

    public List<String> getFileListWithLastModifyTime(String path, String startTime, String endTime) throws SftpException, ParseException {
        SimpleDateFormat pattern = new SimpleDateFormat("yyyyMMdd HHmmss");
        Date startDate;
        Date endDate;
        startDate = pattern.parse(startTime);
        endDate = pattern.parse(endTime);

        Vector<ChannelSftp.LsEntry> result = new Vector();
        ChannelSftp.LsEntrySelector selector = new ChannelSftp.LsEntrySelector() {
            public int select(ChannelSftp.LsEntry lsEntry) {
                int mTime = lsEntry.getAttrs().getMTime();
                int aTime = lsEntry.getAttrs().getATime();
                boolean isDir = lsEntry.getAttrs().isDir();
                Date date = new Date(mTime * 1000L);
                if (!isDir && date.after(startDate) && date.before(endDate)) {
                    result.addElement(lsEntry);
                }
                //     return ChannelSftp.LsEntrySelector.CONTINUE;
                // else
                //     return ChannelSftp.LsEntrySelector.BREAK;
                return ChannelSftp.LsEntrySelector.CONTINUE;
            }
        };
        sftp.ls(path, selector);
        List<String> pathList = result.stream().map(ChannelSftp.LsEntry::getFilename).map(t -> path + t).collect(Collectors.toList());
        result.clear();
        // 继续遍历文件夹
        Vector<ChannelSftp.LsEntry> temp = sftp.ls(path);
        for (ChannelSftp.LsEntry entry : temp) {
            if (entry.getAttrs().isDir() && !entry.getFilename().equals(".") && !entry.getFilename().equals("..")) {
                String depth = path + entry.getFilename();
                sftp.ls(depth, selector);
                pathList.addAll(result.stream().map(ChannelSftp.LsEntry::getFilename).map(t -> depth + "/" + t).collect(Collectors.toList()));
                result.clear();
            }
        }
        // 输出
        return pathList;
    }
}
