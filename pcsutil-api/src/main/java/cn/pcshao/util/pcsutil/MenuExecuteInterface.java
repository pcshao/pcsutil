package cn.pcshao.util.pcsutil;

/**
 * @author pcshao.cn
 * @date 2022/12/3
 */
public interface MenuExecuteInterface {
    void execute();
}
