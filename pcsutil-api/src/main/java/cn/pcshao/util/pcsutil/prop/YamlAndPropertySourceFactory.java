package cn.pcshao.util.pcsutil.prop;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.DefaultPropertySourceFactory;
import org.springframework.core.io.support.EncodedResource;

/**
 * @author pcshao.cn
 * @date 2023/3/3
 */
public class YamlAndPropertySourceFactory extends DefaultPropertySourceFactory {
    public YamlAndPropertySourceFactory() {
    }

    public PropertySource<?> createPropertySource(String name, EncodedResource resource) throws IOException {
        if (resource == null) {
            return super.createPropertySource(name, resource);
        } else {
            Resource resourceResource = resource.getResource();
            if (!resourceResource.exists()) {
                return new PropertiesPropertySource((String)null, new Properties());
            } else if (!resourceResource.getFilename().endsWith(".yml") && !resourceResource.getFilename().endsWith(".yaml")) {
                return super.createPropertySource(name, resource);
            } else {
                List<PropertySource<?>> sources = (new YamlPropertySourceLoader()).load(resourceResource.getFilename(), resourceResource);
                return (PropertySource)sources.get(0);
            }
        }
    }
}
