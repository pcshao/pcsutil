package cn.pcshao.util.pcsutil.ssh.core;

import cn.pcshao.util.pcsutil.MenuExecuteInterface;
import cn.pcshao.util.pcsutil.common.JSchExecutor;
import cn.pcshao.util.pcsutil.prop.YamlAndPropertySourceFactory;
import cn.pcshao.util.pcsutil.ssh.config.SshhelperRetryConfig;
import cn.pcshao.util.pcsutil.ssh.config.SshheplerConfig;
import cn.pcshao.util.pcsutil.ssh.config.Target;
import com.jcraft.jsch.JSchException;
import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 建立SSH连接并批量下载
 * @author pcshao.cn
 * @date 2023/3/1
 */
@Service
@PropertySource(value = "classpath:sshhelper.yml", factory = YamlAndPropertySourceFactory.class)
public class SshDualDownloadHelper implements MenuExecuteInterface {

    @Value("${sshhelper.source.default.port}")
    private int defaultPort;
    @Value("${sshhelper.source.default.location}")
    private String defaultLocation;
    @Value("${sshhelper.source.default.password}")
    private String defaultPassword;
    @Value("${sshhelper.logs.start-time}")
    private String startTime;
    @Value("${sshhelper.logs.end-time}")
    private String endTime;
    @Value("${sshhelper.output.path}")
    private String outputPath;

    private static Logger logger = LoggerFactory.getLogger(SshDualDownloadHelper.class);

    @Resource
    private SshheplerConfig config;
    @Resource
    private SshhelperRetryConfig retryConfig;

    @Resource
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;
    @Resource
    @Qualifier("forkJoinPool")
    private ForkJoinPool forkJoinPool;

    @Override
    public void execute() {
        List<Target> targetList = config.getTarget();
        if (Objects.isNull(targetList))
            throw new RuntimeException("SSH配置文件有误");
        logger.info("默认配置值清单，默认端口：{}，默认密码：{}，默认文件目录：{}", defaultPort, defaultPassword, defaultLocation);
        // 获取目标机器配置清单
        List<Target> shellList = genTargetShellList(targetList, defaultPort, defaultPassword, defaultLocation);
        // JSCH 建立连接，过滤非法连接
        List<Target> realShellList = filterBadShell(shellList);
        // JSCH 拉取到本地
        connect2Shell(realShellList, startTime, endTime);
        // 结束
        // System.exit(1);
    }

    private List<String> connect2Shell(List<Target> realShellList, String startTime, String endTime) {
        logger.info("开始进行下载，进行多线程分发");
        AtomicInteger successCount = new AtomicInteger(0);
        AtomicInteger failedCount = new AtomicInteger(0);
        Vector<String> failedList = new Vector<>();
        // 下载任务的future task
        List<ForkJoinTask<Boolean>> joinTaskList = new ArrayList<>(16 >> 2);
        realShellList.stream().forEach(t -> {
            joinTaskList.add(forkJoinPool.submit(() -> {
                JSchExecutor jSchExecutor = new JSchExecutor(t.getUsername(), t.getPassword(), t.getIp(), t.getPort());
                try {
                    jSchExecutor.connect();
                    jSchExecutor.changeDir(t.getLocation());
                } catch (JSchException e) {
                    logger.error("以下SSH连接建立失败，{}", t, e);
                }
                // 获取文件列表
                try {
                    List<String> pathList = jSchExecutor.getFileListWithLastModifyTime(t.getLocation(), startTime, endTime);
                    // 计数
                    failedCount.addAndGet(pathList.size());
                    successCount.addAndGet(pathList.size());
                    logger.info("目标文件路径列表获取成功，数量{} ip:{} startTime:{} endTime:{}", pathList.size(), t.getIp(), startTime, endTime);
                    for (String item : pathList) {
                        joinTaskList.add(forkJoinPool.submit(() -> {
                            JSchExecutor jSchExecutorInner = new JSchExecutor(t.getUsername(), t.getPassword(), t.getIp(), t.getPort());
                            String targetFile = String.format("%s:%s", t.getIp(), item);
                            long start = System.currentTimeMillis();
                            try {
                                jSchExecutorInner.connect();
                                logger.info("文件开始下载 {}", targetFile);
                                jSchExecutorInner.downloadFile(item, genLocalFilePath(t, item, outputPath));
                            } catch (Exception e) {
                                logger.error("文件下载失败 {}", targetFile, e);
                                failedList.add(targetFile);
                                return false;
                            }
                            long end = System.currentTimeMillis();
                            failedCount.decrementAndGet();
                            logger.info("文件下载成功 {} cost {} ms", targetFile, (end - start));
                            return true;
                        }));
                    }
                } catch (Exception e) {
                    logger.error("下载文件出现未知异常", e);
                    return false;
                }
                return true;
            }));
        });
        // 阻塞获取
        for (int i = 0; i < joinTaskList.size(); i++) {
            try {
                joinTaskList.get(i).get();
            } catch (Exception e) {
                logger.error("阻塞式获取future task时失败", e);
            }
        }
        // 结果输出
        if (failedList.size() > 0) {
            logger.info("全部下载任务执行完毕，成功个数{}，失败个数{}，失败详情{}", successCount.get(), failedCount.get(), failedList);
        } else if (failedCount.get() > 0) {
            logger.error("全部下载任务执行完毕，成功个数{}，失败个数{}，无失败详情，请检查", successCount.get(), failedCount.get());
        } else {
            logger.info("全部下载任务执行完毕，成功个数{}，文件路径 {}", successCount.get(), outputPath);
        }
        return failedList;
    }

    private String genLocalFilePath(Target item, String filePath, String outputPath) {
        int i = filePath.lastIndexOf("/");
        String fileName;
        if (i > 0) {
            fileName = filePath.substring(i + 1);
        } else {
            fileName = filePath;
        }
        if (!new File(outputPath).exists()) {
            new File(outputPath).mkdir();
        }
        return String.format("%s/ssh-%s-%s-%d-%s", outputPath, item.getMark(), item.getIp(), System.currentTimeMillis(), fileName);
    }

    private List<Target> filterBadShell(List<Target> shellList) {
        // 两种多线程分发方式
        // 1. 使用shellList.parallelStream()，默认开启公共forkJoinPool
        // 2. 使用CompletableFuture
        // CompletableFuture<List<Target>> future = CompletableFuture.supplyAsync();
        ForkJoinTask<List<Target>> forkJoinTask = forkJoinPool.submit(() -> {
            return shellList.parallelStream().filter(t -> {
                JSchExecutor jSchExecutor = new JSchExecutor(t.getUsername(), t.getPassword(), t.getIp(), t.getPort());
                boolean retrySuccess = true;
                try {
                    jSchExecutor.connect();
                } catch (Exception e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("以下SSH连接建立失败，{}", t, e);
                    } else {
                        logger.error("以下SSH连接建立失败，{}", t);
                    }
                    Pair<Boolean, JSchExecutor> retryResult = retry(jSchExecutor, t, retryConfig);
                    retrySuccess = retryResult.getKey();
                    jSchExecutor = retryResult.getValue();
                }
                if (retrySuccess) {
                    try {
                        jSchExecutor.changeDir(t.getLocation());
                    } catch (Exception e) {
                        logger.error("以下SSH连接，文件目录有误，{}", t, e);
                        return false;
                    }
                    return true;
                } else {
                    return false;
                }
            }).collect(Collectors.toList());
        });
        try {
            return forkJoinTask.get();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    private Pair<Boolean, JSchExecutor> retry(JSchExecutor jSchExecutor, Target t, SshhelperRetryConfig retryConfig) {
        List<String> retryConfigPassword = retryConfig.getPassword();
        if (CollectionUtils.isEmpty(retryConfigPassword)) {
            return new Pair<>(false, jSchExecutor);
        }
        logger.info("{} 重试配置不为空，即将进行重试", t.getIp());
        int i = 0;
        for (; i < retryConfigPassword.size(); i++) {
            t.setPassword(retryConfigPassword.get(i));
            try {
                jSchExecutor = new JSchExecutor(t.getUsername(), t.getPassword(), t.getIp(), t.getPort());
                jSchExecutor.connect();
            } catch (JSchException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("重试{}次，以下SSH连接建立失败，{}", i+1, t, e);
                } else {
                    logger.error("重试{}次，以下SSH连接建立失败，{}", i+1, t);
                }
                continue;
            }
            logger.info("重试{}次，以下SSH连接建立成功，{}", i+1, t.getIp());
            return new Pair<>(true, jSchExecutor);
        }
        logger.info("重试{}次仍然失败，以下SSH连接 {}", i+1, t.getIp());
        return new Pair<>(false, jSchExecutor);
    }

    private List<Target> genTargetShellList(List<Target> targetList, int defaultPort, String defaultPassword, String defaultLocation) {
        return targetList.stream().map(t -> {
            Target target = new Target();
            BeanUtils.copyProperties(t, target);
            if (target.getPort() == 0) {
                target.setPort(defaultPort);
            }
            if (StringUtils.isEmpty(target.getPassword())) {
                target.setPassword(defaultPassword);
            }
            if (StringUtils.isEmpty(target.getLocation())) {
                target.setLocation(defaultLocation);
            }
            return target;
        }).collect(Collectors.toList());
    }
}
