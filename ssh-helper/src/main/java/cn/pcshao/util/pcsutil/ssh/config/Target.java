package cn.pcshao.util.pcsutil.ssh.config;

import java.io.Serializable;

/**
 * @author pcshao.cn
 * @date 2023/3/1
 */
public class Target implements Serializable {

    private String ip;
    private int port;
    private String mark;
    private String username;
    private String password;
    private String location;

    @Override
    public String toString() {
        return "Target{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                ", mark='" + mark + '\'' +
                ", username='" + username + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
