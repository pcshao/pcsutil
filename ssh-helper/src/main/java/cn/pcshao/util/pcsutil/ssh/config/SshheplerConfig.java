package cn.pcshao.util.pcsutil.ssh.config;

import cn.pcshao.util.pcsutil.prop.YamlAndPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * @author pcshao.cn
 * @date 2023/3/2
 */
@Configuration
@PropertySource(value = "classpath:sshhelper.yml", factory = YamlAndPropertySourceFactory.class)
@ConfigurationProperties(prefix = "sshhelper.source")
public class SshheplerConfig {

    private List<Target> target;

    public List<Target> getTarget() {
        return target;
    }

    public void setTarget(List<Target> target) {
        this.target = target;
    }
}
