package cn.pcshao.util.pcsutil.ssh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadPoolExecutor;

// 加载context，支持工具类
@SpringBootApplication(scanBasePackages = {
        "cn.pcshao.util.pcsutil.ssh"
})
@EnableAsync
public class SshHelperStarter {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SshHelperStarter.class);
        app.run(args);
    }
    /**
     * 线程池配置
     */
    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(10);
        // 设置最大线程数
        executor.setMaxPoolSize(10);
        // 设置队列容量
        executor.setQueueCapacity(5);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(60);
        // 设置默认线程名称
        executor.setThreadNamePrefix("SSH Helper Thread-");
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 等待所有任务结束后再关闭线程池
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }

    /**
     * 结合与parallelStream使用的线程池
     *
     * @return
     */
    @Bean
    public ForkJoinPool forkJoinPool() {
        ForkJoinPool forkJoinPool = new ForkJoinPool(16);
        return forkJoinPool;
    }
}
