package cn.pcshao.util.pcsutil.ssh.config;

import cn.pcshao.util.pcsutil.prop.YamlAndPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * @author pcshao.cn
 * @date 2023/3/3
 */
@Configuration
@PropertySource(value = "classpath:sshhelper.yml", factory = YamlAndPropertySourceFactory.class)
@ConfigurationProperties(prefix = "sshhelper.source.retry")
public class SshhelperRetryConfig {

    private List<String> password;

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }
}
