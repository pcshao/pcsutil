package cn.pcshao.util.pcsutil.ssh;

import cn.pcshao.util.pcsutil.ssh.core.SshDualDownloadHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * @author pcshao.cn
 * @date   2019-10-11
 * 入口
 */
@Component
public class Menu {

    public Menu(@Autowired() SshDualDownloadHelper sshDualDownloadHelper){
        //command console
        notice();
        while (true){
            Scanner scanner = new Scanner(System.in);
            String inputCommand = scanner.next();
            if ("0".equals(inputCommand))
                System.exit(1);
            if ("1".equals(inputCommand))
                sshDualDownloadHelper.execute();
        }
    }

    public static void notice(){
        System.out.println("输入1-开始 0-退出");
    }
}
